require('./bootstrap');

window.Vue = require('vue');

Vue.component('item', require('./components/MkTreeComponent.vue'));
Vue.component('item-add', require('./components/form2addItemComponent.vue'));
Vue.component('item-edit', require('./components/form2editItemComponent.vue'));
Vue.component('item-sel', require('./components/itemSelComponent.vue'));
Vue.component('full-tree', require('./components/fullTreeComponent.vue'));
Vue.component('restrictions-list', require('./components/restrictionListComponent.vue'));
Vue.component('invoice-table', require('./components/invoiceTableComponent.vue'));

let token = document.head.querySelector('meta[name="csrf-token"]');
if (!token) {
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
const appConfig = new Vue({
	el: '#config',
	name: 'appConfig',
	data: {
		topLevel: Object,
		item: Object,
		currentId: Number,
		invoiceTable: {},
		shouldBeDisabled: {},
		userEmail: '',
	},
	beforeMount () {
		this.$nextTick(function () {
			this.getTopLevel()
		})
	},
	watch: {
		item: function () {
			this.getDescendantsById()
		}
	},
	methods: {
		getLimitIds: function (subj_id) {
			axios.get('/api/getLimitIds/'+subj_id)
			.then(response => {
				this.$set(this, 'shouldBeDisabled', {'ban':response.data})
			})
			.catch(error => console.log(error))
		},
		onAdd2Invoice: function (payload) {
			// Fill invoiceTable object 
			this.$set(this.invoiceTable, payload.data.id, payload)
			this.getLimitIds(payload.data.id)
		},
		getDescendantsById: function () {
			if(this.currentId) {
				axios.get('/api/getDescendantsTruncated/'+this.currentId)
				.then(response => this.$set(this.item, 'hasChild', response.data))
				.catch(error => console.log(error))
			}
		},
		sendInvoiceData2host: function (mode, email = '') {
			var invoiceTableLength = Object.keys(this.invoiceTable).length
			var totalPrice = 0
			if (invoiceTableLength > 0) {
				for(var key in this.invoiceTable) {
					totalPrice += this.invoiceTable[key].data.price
				}
				axios.post('/api/prepareInvoice', {
					'mode': mode,
					'email': email,
					'item': this.item,
					'parts': this.invoiceTable,
					'totalPrice': totalPrice
				})
				.then(response => console.log(response))
				.catch(error => console.log(error))
			}
		},
		onPrintInvoice: function () {
			this.sendInvoiceData2host('print')
		},
		onEmailInvoice: function () {
			// спросить email, передать его на сервер, пока 'mavikon@gmail.com'
			this.sendInvoiceData2host('email', this.userEmail)
		},
		onResetInvoice: function () {
			this.getTopLevel()
			this.invoiceTable = {}
			this.shouldBeDisabled = {}
		},
		sectionName: function (par) {
			this.currentId = par
			if(this.topLevel[this.currentId]) {
				this.$set(this, 'item', this.topLevel[this.currentId])
				if(this.item['price']) {
					this.$set(this.invoiceTable, 0, {
						'data': this.item,
						'name': this.item.description
					})
				}
				return this.item.name
			}
			return this.currentId
		},
		getTopLevel: function () {
			axios.get('/api/getTopLevelTruncated')
			.then(response => this.$set(this, 'topLevel', response.data))
			.catch(error => console.log(error))
		},		
	},
});
