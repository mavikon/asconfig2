
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('item', require('./components/MkTreeComponent.vue'));
Vue.component('item-add', require('./components/form2addItemComponent.vue'));
Vue.component('item-edit', require('./components/form2editItemComponent.vue'));
Vue.component('item-sel', require('./components/itemSelComponent.vue'));
Vue.component('full-tree', require('./components/fullTreeComponent.vue'));
Vue.component('restrictions-list', require('./components/restrictionListComponent.vue'));
Vue.component('invoice-table', require('./components/invoiceTableComponent.vue'));

let token = document.head.querySelector('meta[name="csrf-token"]');
if (!token) {
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

const app = new Vue({
	el: '#app',
	data: {
		topLevel: Object,
		item: Object,
		newItemForm: false,
		currentId: Number,
		restrictionsArray: [],
		limiterSubj: 0,
		updRestrictionList: 0,
	},
	beforeMount () {
		this.$nextTick(function () {
			this.getTopLevel()
		})
	},
	computed: {
	},
	watch: {
		item: function () {
			this.getDescendantsById()
		}
	},

	methods: {
		onSaveRestrict: function () {
			if (this.restrictionsArray.length > 0 
				&& this.limiterSubj > 0 
				&& !this.restrictionsArray.includes(this.limiterSubj)) {
				axios.post('/api/saveRestrictions', {
					'topId': this.item.id,
					'limiterSubj': this.limiterSubj, 
					'restrictionsArray': this.restrictionsArray
				})
				.then(response => {
					this.updRestrictionList += 1
				})
				.catch(error => console.log(error))
			}
			this.onCancelRestrict()
		},
		onCancelRestrict: function () {
			this.restrictionsArray = []
			this.limiterSubj = 0
		},
		onCheckboxChange: function (payload) {
			if(this.restrictionsArray.includes(payload)) {
				//remove it from array
				this.restrictionsArray.splice(
					this.restrictionsArray.findIndex(
						element => element == payload
					), 1
				)
			} else {
				//add it to array
				this.restrictionsArray.push(payload)
			}
		},
		onRadioChange: function (payload) {
			this.limiterSubj = payload
		},
		getDescendantsById: function () {
			if(this.currentId) {
				axios.get('/api/getDescendants/'+this.currentId)
				.then(response => this.$set(this.item, 'hasChild', response.data))
				.catch(error => console.log(error))
			}
		},
		getTopLevel: function () {
			axios.get('/api/getTopLevel')
			.then(response => this.$set(this, 'topLevel', response.data))
			.catch(error => console.log(error))
		},
		sectionName: function (par) {
			this.currentId = par
			if(this.topLevel[this.currentId]) {
				this.$set(this, 'item', this.topLevel[this.currentId])
				return this.item.name
			}
			return this.currentId
		},
	},
});
