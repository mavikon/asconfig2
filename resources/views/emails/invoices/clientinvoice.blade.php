<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>Выбранная конфигурация {{ $invoice['item']['name'] }}</title>
</head>
<body>
<div>
	<h3>Выбранная конфигурация {{ $invoice['item']['name'] }}</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th class="col-xs-1">#</th>
				<th class="col-xs-8">Наименование</th>
				<th class="col-xs-3 text-right">Цена</th>
			</tr>
		</thead>
		<tbody>
			@foreach($invoice['parts'] as $part)
			<tr>
				<td class="col-xs-1">{{ $loop->iteration }}</td>
				<td class="col-xs-8">{{ $part['name'] }}: {{ $part['data']['name'] }}</td>
				<td class="col-xs-3 text-right">{{ number_format($part['data']['price'], 2) }} руб.</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr class="success">
				<td colspan="2" class="col-xs-8">Общая стоимость в Руб.(вкл.НДС):</td>
				<td class="col-xs-4 text-right">{{ number_format($invoice['totalPrice'], 2) }}</td>
			</tr>
		</tfoot>
	</table>
	<p>
		Указанная цена не является публичной офертой. Итоговая стоимость может отличаться.
		Для приобретения СХД или уточнения необходимой информации просим обращаться по
		телефону, 8 (495) 984-60-73 или по электронной почте info@asntl.ru.
		Наш адрес: г. Москва, Валдайский проезд, д.16, стр.1, офис 246.
		*Предложение не является публичной офертой.
	</p>
</div>
</body>
</html>
