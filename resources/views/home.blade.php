@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<button
						type="button" class="btn btn-default"
						@click="newItemForm = !newItemForm">Create Root</button>
					<item-add :item="{}" :new-item-form="newItemForm"
						v-on:update-parent="getTopLevel()"
						v-on:shut-me-please="newItemForm = false"></item-add>
				</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					<div>
						<item v-for="item in topLevel" :key="item.id" :item="item" :depth=3 :item-control=true></item>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection
