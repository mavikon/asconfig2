@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p><span v-text="'Формирование ограничений для ' + sectionName({{ $id }})"></span></p>
					<div class="row">
						<div class="col-xs-8">
							<p>Отметьте слева вариант, с которым нельзя будет выбрать варианты, отмеченные справа</p>
						</div>
						<div class="col-xs-4 text-right">
							<button type="button" class="btn btn-primary"
								@click="onSaveRestrict">Save</button>
							<button type="button" class="btn btn-default"
								@click="onCancelRestrict">Cancel</button>
						</div>
					</div>
				</div>
				<div class="panel-body row ">
					<div class="restriction-boxes">
						<div class="col-xs-6 restriction-box">
							<full-tree
								:item-id="{{ $id }}"
								:mode="'radio'"
								:limiter-subj="limiterSubj"
								@radio-subj-id="onRadioChange($event)">
							</full-tree>
						</div>
						<div class="col-xs-6 restriction-box">
							<full-tree
								:item-id={{ $id }}
								:mode="'checkbox'"
								:restrictions-array="restrictionsArray"
								@checkbox-restrictions="onCheckboxChange($event)">
							</full-tree>
						</div>
					</div>
					<div class="restriction-list">
						{{-- вывод ограничений в виде таблицы с возможностью удаления строк --}}
						<restrictions-list :top-id="{{ $id }}" :upd="updRestrictionList"></restrictions-list>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection
