<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'AS Configurator') }}</title>

	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('fa/css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
<div id="config">
<!-- Modal -->
<div class="modal fade" id="askEmailModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Введите email</h4>
			</div>
			<div class="modal-body">
				<input type="email" class="form-control input-md" id="useremail"
				v-model="userEmail"
				placeholder="example@example.com">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal" @click="onEmailInvoice">Отправить</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="inline-block">
						<span v-text="'Расчет конфигурации ' + sectionName({{ $id }})"></span>
					</div>
					<div class="btn-group pull-right inline-block">
						<button type="button" class="btn btn-primary disabled" @click="onPrintInvoice">Печать</button>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#askEmailModal">E-mail</button>
						<button type="button" class="btn btn-primary" @click="onResetInvoice">Сброс</button>
					</div>
				</div>
				<div class="panel-body row">
					<div class="col-xs-6">
						<item-sel v-for="child in item.hasChild" :key="child.id"
							:item="child" :disabled-by-rule="shouldBeDisabled"
							v-on:add-to-invoice="onAdd2Invoice" ></item-sel>
					</div>
					<div class="col-xs-6">
						<invoice-table :invoice-item="invoiceTable"></invoice-table>
						<div class="promocode">
							<label for="promocode">Промокод <span class="labels">(если есть)</span>: </label>
							<input type="text" class="form-inline input-sm" name="promocode" id="promocode">
							<button type="button" class="btn btn-primary">Ввод</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

</div>
	<!-- Scripts -->
	<script src="{{ asset('js/appConfig.js') }}"></script>
</body>
</html>
