<div>
	<div v-if="newItemForm" class="wraper row">
		<div class="form-group col-xs-6">
			<label for="itemname">Name:</label>
			<input type="text" class="form-control" id="itemname">
		</div>
		<div class="form-group col-xs-3">
			<label for="itemname">Level:</label>
			<input type="text" class="form-control" id="itemlevel">
		</div>
		<div class="form-group col-xs-3">
			<label for="itemname">Order:</label>
			<input type="text" class="form-control" id="itemorder">
		</div>
		<div class="form-group col-xs-12">
			<label for="description">Description:</label>
			<textarea class="form-control" rows="4" id="description"></textarea>
		</div>
		<div class="form-group col-xs-12">
			<button type="button" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-default">Cancel</button>
		</div>
	</div>
</div>
