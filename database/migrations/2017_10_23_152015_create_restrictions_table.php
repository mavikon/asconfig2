<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestrictionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('restrictions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('top_id')->unsigned();
			$table->integer('subj_id')->unsigned();
			$table->integer('limit_id')->unsigned()->nullable();
			$table->timestamps();
			$table->foreign('subj_id')->references('id')->on('items')->onDelete('cascade');
			$table->foreign('limit_id')->references('id')->on('items')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('restrictions');
	}
}
