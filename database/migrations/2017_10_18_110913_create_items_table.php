<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('items', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->integer('order')->nullable();
			$table->integer('item_id')->unsigned()->nullable();
			$table->string('manager_email')->nullable();
			$table->string('partnum')->nullable();
			$table->float('price', 12, 2)->default(0);
			$table->float('cost', 12, 2)->default(0);
			$table->float('factor', 12, 3)->default(1.0);
			$table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
			$table->timestamps();
		});
		DB::unprepared("
			create trigger `check_non_unique_names_trigger` before insert on `items`
			for each row
			begin
				IF new.`name` IN (
					select c.`name`
					from `items` c
					where (NEW.`name` = c.`name` AND IFNULL(NEW.`item_id`,0) = IFNULL(c.`item_id`,0))
				) THEN
				SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = 'ОШИБКА - Попытка добавить запись с именем, которое уже существует в том же разделе или категории!';
				end if;
			end;");

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::unprepared('DROP TRIGGER `check_non_unique_names_trigger`');
		Schema::dropIfExists('items');
	}
}
