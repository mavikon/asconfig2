<?php

use App\Models\Item;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$this->call('AsConfigSeeder');
		$this->command->info('AsConfig seeds finished.');
	}
}
class AsConfigSeeder extends Seeder {
	public function run() {
		// clear database
		DB::table('items')->delete();

		$l1 = Item::create([
			'name' => 'ДСХД',
			'description' => 'Конфигурация системы хранения данных',
			'order' => 0, 'item_id' => null]);
		$l2 = Item::create([
			'name' => 'Lorem',
			'description' => 'Tum Torquatus: Prorsus, inquit, assentior;',
			'order' => 0, 'item_id' => null]);
		$l3 = Item::create([
			'name' => 'Ipsum',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => null]);

		$l1_1 = Item::create([
			'name' => 'Lorem Ipsum 1',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => $l1->id]);
		$l1_2 = Item::create([
			'name' => 'Lorem Ipsum 2',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => $l1->id]);
		$l1_3 = Item::create([
			'name' => 'Lorem Ipsum 3',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => $l1->id]);

		$l1_1_1 = Item::create([
			'name' => 'lorem ipsum 1 - 1 - 1',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => $l1_1->id]);
		$l1_1_2 = Item::create([
			'name' => 'lorem ipsum 1 - 1 - 2',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => $l1_1->id]);
		$l1_1_3 = Item::create([
			'name' => 'lorem ipsum 1 - 1 - 3',
			'description' => 'Beatum, inquit. Non est igitur voluptas bonum.',
			'order' => 0, 'item_id' => $l1_1->id]);
	}
}
