<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/api/getTopLevel', 'Api\ItemController@getTopLevel');
Route::get('/api/getTopLevelTruncated', 'Api\ItemController@getTopLevelTruncated');
Route::post('/api/saveNewItem', 'Api\ItemController@saveNewItem');
Route::post('/api/updateNewItem', 'Api\ItemController@updateItem');
Route::delete('/api/deleteItem', 'Api\ItemController@deleteItem');
Route::get('/api/getDescendants/{id}', 'Api\ItemController@getDescendants');
Route::get('/api/getDescendantsTruncated/{id}', 'Api\ItemController@getDescendantsTruncated');
Route::get('/config/{id}', 'ItemController@config');
Route::get('/restrictions/{id}', 'RestrictionController@createNew');
Route::get('/api/getAllDescendants/{id}', 'Api\ItemController@getAllDescendants');
Route::post('/api/saveRestrictions', 'Api\RestrictionController@saveRestrictions');
Route::get('/api/getLimitIds/{subj_id}', 'Api\RestrictionController@getLimitIds');
Route::get('/api/getRestrictions/{id}', 'Api\RestrictionController@getRestrictions');
Route::delete('/api/deleteRestriction', 'Api\RestrictionController@deleteRestriction');

Route::post('/api/prepareInvoice', 'Api\InvoiceController@prepareMail');
