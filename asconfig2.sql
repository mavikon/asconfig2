-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: asconfig2
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'ДСХД','',0,NULL,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:06:39','2017-10-26 21:06:39'),(2,'Тип корпуса','',0,1,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:07:16','2017-10-26 21:07:16'),(3,'Количество HDD','',0,1,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:07:27','2017-10-26 21:07:27'),(4,'CPU','',0,1,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:07:38','2017-10-26 21:07:38'),(5,'RAM','',0,1,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:07:50','2017-10-26 21:07:50'),(6,'Cache','',0,1,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:08:03','2017-10-26 21:08:03'),(7,'Операционная система','',0,1,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:08:14','2017-10-26 21:08:14'),(8,'1U','',0,2,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:08:30','2017-10-26 21:08:30'),(9,'2U','',0,2,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:08:38','2017-10-26 21:08:38'),(10,'4U','',0,2,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:08:44','2017-10-26 21:08:44'),(11,'4x HDD','',0,3,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:08:57','2017-10-26 21:08:57'),(12,'8x HDD','',0,3,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:09:07','2017-10-26 21:09:07'),(13,'12x HDD','',0,3,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:09:17','2017-10-26 21:09:17'),(14,'24x HDD','',0,3,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:09:26','2017-10-26 21:09:26'),(15,'36x HDD','',0,3,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:09:36','2017-10-26 21:09:36'),(16,'1x Xeon E3','',0,4,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:09:49','2017-10-26 21:09:49'),(17,'2x Xeon E3','',0,4,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:10:01','2017-10-26 21:10:01'),(18,'1x Xeon E5','',0,4,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:10:12','2017-10-26 21:10:12'),(19,'2x Xeon E5','',0,4,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:10:20','2017-10-26 21:10:20'),(20,'8 Gb','',0,5,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:10:31','2017-10-26 21:10:31'),(21,'16 Gb','',0,5,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:10:46','2017-10-26 21:10:46'),(22,'32 Gb','',0,5,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:10:56','2017-10-26 21:10:56'),(23,'64 Gb','',0,5,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:11:06','2017-10-26 21:11:06'),(24,'Нет','',0,6,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:11:37','2017-10-26 21:11:37'),(25,'32 Gb','',0,6,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:11:46','2017-10-26 21:11:46'),(26,'64 Gb','',0,6,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:11:55','2017-10-26 21:11:55'),(27,'128 Gb','',0,6,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:12:03','2017-10-26 21:12:03'),(28,'Нет','',0,7,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:12:15','2017-10-26 21:12:15'),(29,'AS-Engine','',0,7,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:12:27','2017-10-26 21:12:27'),(30,'Linux','',0,7,NULL,NULL,0.00,0.00,1.000,'2017-10-26 21:12:38','2017-10-26 21:12:38');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`neis2aip`@`localhost`*/ /*!50003 trigger `check_non_unique_names_trigger` before insert on `items`
			for each row
			begin
				IF new.`name` IN (
					select c.`name`
					from `items` c
					where (NEW.`name` = c.`name` AND IFNULL(NEW.`item_id`,0) = IFNULL(c.`item_id`,0))
				) THEN
				SIGNAL SQLSTATE '45000'
					SET MESSAGE_TEXT = 'ОШИБКА - Попытка добавить запись с именем, которое уже существует в том же разделе или категории!';
				end if;
			end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_10_18_110913_create_items_table',1),(4,'2017_10_23_152015_create_restrictions_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `restrictions`
--

LOCK TABLES `restrictions` WRITE;
/*!40000 ALTER TABLE `restrictions` DISABLE KEYS */;
INSERT INTO `restrictions` VALUES (1,1,8,12,'2017-10-26 21:13:17','2017-10-26 21:13:17'),(2,1,8,13,'2017-10-26 21:13:17','2017-10-26 21:13:17'),(3,1,8,14,'2017-10-26 21:13:17','2017-10-26 21:13:17'),(4,1,8,15,'2017-10-26 21:13:17','2017-10-26 21:13:17'),(5,1,9,14,'2017-10-26 21:13:22','2017-10-26 21:13:22'),(6,1,9,15,'2017-10-26 21:13:22','2017-10-26 21:13:22');
/*!40000 ALTER TABLE `restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Maksim','mavikon@gmail.com','$2y$10$5kGiR1jSgTZoRv8DV0w9V.h.bjGX4vDG6cKm.Ox.I4GELj8rJEkpG',NULL,'2017-10-26 21:06:12','2017-10-26 21:06:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-27  3:58:04
