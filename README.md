# README #

## Configuring MySQL ##

* Log into the MySQL root administrative account.
```
$ mysql -u root -p
```

You will be prompted for the password you set for the MySQL root account during installation.

* Start by creating a new database called `aesldb` (or whatever name you want), which is what we'll use for the website. You can choose a different name, but make sure to remember it because you'll need it later.
```mysql
CREATE DATABASE <dbname> DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```

* Next, create a new user that will be allowed to access this database. Here we use `laraveluser` as the username, but you can customize this too. Remember to replace `password` in the following line with a strong and secure password.
```mysql
GRANT ALL ON <dbname>.* TO 'laraveluser'@'localhost' IDENTIFIED BY 'password';
```

* Flush the privileges to notify the MySQL server of the changes.
```mysql
FLUSH PRIVILEGES;
```

* And exit MySQL 

```mysql
EXIT;
```

## Setting Up the Application ##

* Clone/download repository:

```
$ git clone git@gitlab.com:mavikon/asconfig2.git
```

* Change working directory
```
$ cd asconfig2
```

* Next, we need to install the project dependencies. Laravel utilizes Composer to handle dependency management, which makes it easy to install the necessary packages in one go.
```
$ composer install
```

### Configuring the Application Environment ###

* Edit configuration file 
```
$ vim .env
```

```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:CUJnpAfsHqFTt8erYiYaXcGyWk+VEZAKXIweyQys6nw=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dbname
DB_USERNAME=username
DB_PASSWORD=secret

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHE
R_APP_SECRET=
```

setting the database: databasename, username, password, etc. Save `.env` file and exit.

* Generate application key:

```
$ php artisan key:generate
```

* Next, we have to run database migrations, which will populate the newly created database with necessary tables for the application to run properly.

```
$ php artisan migrate
```

* to start serving application locally run:

```
php artisan serve
```

### Deployment instructions ###

* Deployment instruction can be found here : 

[ how to deploy a laravel application with nginx on Ubuntu 16.04 ](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-laravel-application-with-nginx-on-ubuntu-16-04)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin [ mavikon@gmail.com ](mailto:mavikon@gmail.com) 
* Other community or team contact
