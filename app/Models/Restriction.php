<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restriction extends Model {
	protected $fillable = ['subj_id', 'limit_id', 'top_id'];
	public function itemBySubj() {
		return $this->belongsTo('App\Models\Item', 'subj_id');
	}
	public function itemByLimit() {
		return $this->belongsTo('App\Models\Item', 'limit_id');
	}
	public function subjName() {
		return $this->hasOne('App\Models\Item', 'id', 'subj_id');
	}
	public function limitName() {
		return $this->hasOne('App\Models\Item', 'id', 'limit_id');
	}
}
