<?php

namespace App\Models;

use App\Models\Item;
use Illuminate\Database\Eloquent\Model;

class Item extends Model {
	protected $fillable = ['name', 'description', 'level',
		'manager_email', 'partnum', 'price', 'cost', 'factor',
		'order', 'item_id'];
	public function parent() {
		return $this->belongsTo('App\Models\Item', 'item_id');
	}
	public function childs() {
		return $this->hasMany('App\Models\Item', 'item_id');
	}
	public function descendants() {
		$arr = [];
		$descendants = [];
		$items = $this->childs();
		foreach ($items->get()->all() as $value) {
			$descendants = Item::find($value->id)->descendants();
			if ($descendants) {
				$value->descendants = $descendants;
			}
			array_push($arr, $value);
		}
		return $arr;
	}
}
