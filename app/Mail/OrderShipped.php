<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable {
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->from(['address' => 'admin@asconfig2.com', 'name' => 'AS Config v.2 Admin'])
			->subject('Invoice from AS Config Bot')
			->text('emails.orders.shipped_plain')
			->view('emails.orders.shipped');
	}
}
