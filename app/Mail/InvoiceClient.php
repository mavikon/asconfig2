<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceClient extends Mailable {
	use Queueable, SerializesModels;

	public $invoice;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($invoice) {
		$this->invoice = $invoice;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->from(['address' => 'ascbot@asntl.ru', 'name' => 'AS Configurator Bot'])
			->subject('Расчет конфигурации ' . $this->invoice['item']['name'])
			->view('emails.invoices.clientinvoice');
	}
}
