<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\InvoiceClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller {
	public function printInvoice(Request $request) {
		return $request->all();
	}
	public function emailInvoice(Request $request) {
		$input = $request->all();
		Mail::to($input['email'])->queue(new InvoiceClient($input));
		return $input;
	}
	public function prepareMail(Request $request) {
		$mode = $request->input('mode');
		if ($mode == 'print') {
			return $this->printInvoice($request);
		} else if ($mode == 'email') {
			return $this->emailInvoice($request);
		}
		return null;
		// look at the $input['mode'], if 'print' then
		// prepare view and show in the browser for client and send extended mail for manager
		// if 'email', then
		// prepare and send two mailables - one for client and one for manager
		// client's email in $input['email'], manager's email in $input['item']['manager_email']

		// Mail::to('max@florin.ru')->queue(new InvoiceClient($input));
		//return new InvoiceClient($input);

	}
}
