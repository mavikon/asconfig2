<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Restriction;
use Illuminate\Http\Request;

class RestrictionController extends Controller {
	public function deleteRestriction(Request $request) {
		$input = $request->all();
		return Restriction::find($input['id'])->delete() ? 'OK' : 'Error';
	}
	public function getRestrictions($id) {
		return Restriction::where('top_id', $id)
			->get()
			->each(function ($restriction, $key) {
				$restriction['subjParentName'] = $restriction->subjName->parent->name;
				$restriction['limitParentName'] = $restriction->limitName->parent->name;
				$restriction['subjName'] = $restriction->subjName->name;
				$restriction['limitName'] = $restriction->limitName->name;
			});
	}
	public function getLimitIds($id) {
		return Restriction::where('subj_id', $id)->pluck('limit_id')
			->union(Restriction::where('limit_id', $id)->pluck('subj_id'));
	}
	public function saveRestrictions(Request $request) {
		$input = $request->all();
		foreach ($input['restrictionsArray'] as $limit_id) {
			Restriction::create([
				'top_id' => $input['topId'],
				'subj_id' => $input['limiterSubj'],
				'limit_id' => $limit_id,
			]);
		}
		return $input;
	}
}
