<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller {
	public function updateItem(Request $request) {
		$input = $request->all();
		// Update database here
		$item = Item::find($input['id']);
		$item->name = empty($input['name']) ? 'Foo' : $input['name'];
		$item->description = empty($input['description']) ? '' : $input['description'];
		$item->order = empty($input['order']) ? 0 : $input['order'];
		$item->manager_email = empty($input['manager_email']) ? '' : $input['manager_email'];
		$item->partnum = empty($input['partnum']) ? '' : $input['partnum'];
		$item->price = empty($input['price']) ? 0 : $input['price'];
		$item->cost = empty($input['cost']) ? 0 : $input['cost'];
		$item->factor = empty($input['factor']) ? 1 : $input['factor'];

		return $item->save() ? 'update item success' : 'update item failed';
	}
	public function saveNewItem(Request $request) {
		$input = $request->all();
		return Item::create([
			'name' => empty($input['name']) ? 'Foo' : $input['name'],
			'description' => empty($input['description']) ? '' : $input['description'],
			'order' => empty($input['order']) ? 0 : $input['order'],
			'manager_email' => empty($input['manager_email']) ? '' : $input['manager_email'],
			'partnum' => empty($input['partnum']) ? '' : $input['partnum'],
			'price' => empty($input['price']) ? 0 : $input['price'],
			'cost' => empty($input['cost']) ? 0 : $input['cost'],
			'factor' => empty($input['factor']) ? 1 : $input['factor'],
			'item_id' => empty($input['itemId']) ? null : $input['itemId']]);
	}
	public function getTopLevelTruncated() {
		return Item::where('item_id', null)->get([
			'id', 'name', 'description', 'order', 'price', 'item_id',
		])->keyBy('id');
	}
	public function getTopLevel() {
		return Item::where('item_id', null)->get()->keyBy('id');
	}
	public function getDescendants($id) {
		return Item::where('item_id', $id)->get()->keyBy('id');
	}
	public function getDescendantsTruncated($id) {
		return Item::where('item_id', $id)->get([
			'id', 'name', 'description', 'order', 'price', 'item_id',
		])->keyBy('id');
	}
	public function getAllDescendants($id) {
		return Item::find($id)->descendants();
	}
	public function deleteItem(Request $request) {
		$input = $request->all();
		return Item::where('id', $input['id'])->delete();
	}
}
